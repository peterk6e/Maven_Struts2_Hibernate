package com.msh.converter;

import org.joda.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;


@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate locDate) {
        return (locDate == null ? null : Date.valueOf(String.valueOf(locDate)));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date sqlDate) {

        //if(sqlDate == null) return null;
        return  new LocalDate(sqlDate);
    }
}