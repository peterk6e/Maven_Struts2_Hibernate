package com.msh.Interceptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.msh.model.User;
import org.apache.struts2.ServletActionContext;


import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import static com.opensymphony.xwork2.Action.LOGIN;


@SuppressWarnings("serial")
public class RoleInterceptor extends AbstractInterceptor {

    private List<String> allowedRoles = new ArrayList<String>();
    private List<String> disallowedRoles = new ArrayList<String>();
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setAllowedRoles(String roles) {
        this.allowedRoles = stringToList(roles);
    }

    public void setDisallowedRoles(String roles) {
        this.disallowedRoles = stringToList(roles);
    }

    public String intercept(ActionInvocation invocation) throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response = ServletActionContext.getResponse();
        String result = null;
        if (!isAllowed(request, invocation.getAction())) {
            result = handleRejection(invocation, response);
        } else {
            result = invocation.invoke();
        }
        return result;
    }

    /**
     * Splits a string into a List
     */
    protected List<String> stringToList(String val) {
        if (val != null) {
            String[] list = val.split("[ ]*,[ ]*");
            return Arrays.asList(list);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * Determines if the request should be allowed for the action
     *
     * @param request The request
     * @param action The action object
     * @return True if allowed, false otherwise
     */
    protected boolean isAllowed(HttpServletRequest request, Object action) {

        System.out.println("inside ROLE interceptor");

        HttpSession session=request.getSession(false);
        boolean result = false;
        User user=null;
        if(session!=null){
            user=(User)session.getAttribute("user");
        }

        if (allowedRoles.size() > 0) {
            if(session==null || user==null){
                return result;
            }
            for (String role : allowedRoles) {
                if (role.equalsIgnoreCase(user.getRole())) {
                    result = true;
                }
            }
            return result;
        } else if (disallowedRoles.size() > 0) {
            if(session!=null || user!=null){
                for (String role : disallowedRoles) {
                    if (role.equalsIgnoreCase(user.getRole())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Handles a rejection by sending a 403 HTTP error
     *
     * @param invocation The invocation
     * @return The result code
     * @throws Exception
     */
    protected String handleRejection(ActionInvocation invocation,
                                     HttpServletResponse response)
            throws Exception {

//        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        msg = "Invalid access right";
        return LOGIN;
    }

}