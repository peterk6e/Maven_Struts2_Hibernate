package com.msh.Interceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.msh.action.JsonAction;
import com.msh.model.User;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.struts2.json.annotations.JSON;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;


public class AuthenticationInterceptor implements Interceptor {

    private static final long serialVersionUID = -5011962009065225959L;

    @Override
    public void destroy() { }

    @Override
    public void init() { }

    @Override
    public String intercept(ActionInvocation actionInvocation)
            throws Exception {
        System.out.println("inside AUTHENTICATION interceptor");

        Map<String, Object> sessionAttributes = actionInvocation.getInvocationContext().getSession();

        User user = (User) sessionAttributes.get("user");


        if(user == null){
            /*return Action.LOGIN;*/
            return INPUT; //issue here : result is not sent to struts.xml..
        }else{
            return actionInvocation.invoke();
        }
    }
}