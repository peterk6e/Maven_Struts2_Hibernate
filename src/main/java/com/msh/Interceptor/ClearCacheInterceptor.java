package com.msh.Interceptor;

import com.msh.model.User;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;
import org.apache.struts2.dispatcher.SessionMap;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static com.opensymphony.xwork2.Action.LOGIN;

public class ClearCacheInterceptor  implements Interceptor {
    private SessionMap<String, Object> sessionMap;

    public SessionMap<String, Object> getSessionMap() {
        return sessionMap;
    }



    public String intercept(ActionInvocation invocation)throws Exception {

        String status= "ENABLED";

        System.out.println("inside CLEAR CACHE interceptor :: " + status );

        // before invoke is pre-action processing
        // after invoke is post-action processing

        if(status.equals("ENABLED")) {
            HttpServletResponse response = ServletActionContext.getResponse();
            System.out.println("******clear cache********");
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
        }


        String result = invocation.invoke(); // the render is done before the invoke(),
                                            // clear cache in post process is useless...

        return result;
    }

    public void destroy() {
    }

    public void init() {
    }
}