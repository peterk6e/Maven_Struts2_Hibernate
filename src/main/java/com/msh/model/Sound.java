package com.msh.model;

import com.msh.converter.LocalDateAttributeConverter;
import org.joda.time.LocalDate;

import javax.persistence.*;



@Entity
@Table(name = "sound", schema = "public")
public class Sound {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sound_generator")
    @SequenceGenerator(name="sound_generator", sequenceName = "sound_sound_id_seq", allocationSize=1)
    @Column(name = "SOUND_ID")
    private Integer sound_id;

    @Basic
    @Column(name = "sound_name", length = 40) //, nullable = false
    private String sound_name;

    @Basic
    @Column(name = "comment", length = 40) //, nullable = false
    private String comment;

    @Basic
    @Convert(converter = LocalDateAttributeConverter.class)
    @Column(name = "import_date")
    private LocalDate import_date;

    //@Lob
    @Basic
    @Column(name = "sound")//, nullable = false
    private byte[] sound;

    @OneToOne
    @JoinColumn(name = "user_id",
            foreignKey = @ForeignKey(name = "fk_sound_user"))
    private User user;

    @Basic
    @Column(name = "artist", length = 40) //, nullable = false
    private String artist;

    @Basic
    @Column(name = "album", length = 40) //, nullable = false
    private String album;

    @Basic
    @Column(name = "sound_location", length = 60) //, nullable = false
    private String sound_location;

    @Basic
    @Column(name = "image_location", length = 60) //, nullable = false
    private String image_location;

    @Basic
    @Column(name = "style", length = 60) //, nullable = false
    private String style;

    @Basic
    @Column(name = "views") //, nullable = false
    private int views;

    public Sound() {
    }

    public Sound(String sound_name, String comment, LocalDate import_date, byte[] sound, User user) {
        this.sound_name = sound_name;
        this.comment = comment;
        this.import_date = import_date;
        this.sound = sound;
        this.user = user;
    }


    public Integer getSound_id() {
        return sound_id;
    }

    public void setSound_id(Integer sound_id) {
        this.sound_id = sound_id;
    }


    public String getSound_name() {
        return sound_name;
    }

    public void setSound_name(String sound_name) {
        this.sound_name = sound_name;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public LocalDate getImport_date() {
        return import_date;
    }

    public void setImport_date(LocalDate import_date) {
        this.import_date = import_date;
    }


    public byte[] getSound() {
        return sound;
    }

    public void setSound(byte[] sound) {
        this.sound = sound;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getSound_location() {
        return sound_location;
    }

    public void setSound_location(String sound_location) {
        this.sound_location = sound_location;
    }

    public String getImage_location() {
        return image_location;
    }

    public void setImage_location(String image_location) {
        this.image_location = image_location;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}

