package com.msh.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
    @SequenceGenerator(name="user_generator", sequenceName = "user_user_id_seq", allocationSize=1)
    @Column(name = "USER_ID", nullable = false, length = 10)//@JoinColumn(name = "USER_ID", nullable = false)
    private Integer user_id;

    @Basic
    @Column(name = "password", nullable = false, length = 40)
    private String password;

    @Basic
    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @Basic
    @Column(name = "email", nullable = false, length = 40)
    private String email;

    @Basic
    @Column(name = "role", nullable = false, length = 40)
    private String role;


    public User(){}

    public User (String name, String  password, String  email){
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public User (String name, String  password, String  email, String  role){
        this.name = name;
        this.password = password;
        this.email = email;
        this.role = role;
    }


    public Integer getUser_id() {
        return user_id;
    }
    public void setUser_id(Integer userId) {
        this.user_id = userId;
    }


    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }



    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (user_id != null ? !user_id.equals(user.user_id) : user.user_id != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {

        int result = user_id != null ? user_id.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
