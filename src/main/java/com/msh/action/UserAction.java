package com.msh.action;

import com.msh.dao.UserDAO;
import com.msh.dao.UserDAOImpl;
import com.msh.listener.HibernateUtils;
import com.msh.model.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.SessionFactory;

import java.util.Map;

public class UserAction extends ActionSupport implements SessionAware {

    private Map<String, Object> sessionMap;
    private String name;
    private String password;
    private String email;
    private String role;
    private String msg;
    private User currentUser;
    private String emailDuplicated;

    private String pageName;


    @Override
    public void setSession(Map<String, Object> map) {
        sessionMap = map;
    }

    public String execute() throws Exception {
        currentUser = (User) sessionMap.get("user");
        setName(currentUser.getName());
        setPassword(currentUser.getPassword());
        setEmail(currentUser.getEmail());
        setRole(currentUser.getRole());
        return SUCCESS;
    }

    public String updateInfoUser() throws Exception {
        currentUser = (User) sessionMap.get("user");

        SessionFactory sf = HibernateUtils.getSessionFactory();
        UserDAO userDAO = new UserDAOImpl(sf);

        User updatedUser = new User(name, password, email);
        userDAO.updateInfoUser(currentUser, updatedUser);
        msg = "info updated";
        return SUCCESS;
    }

    public String createUser() throws Exception {
        if (checkEmailDuplication()) {
            SessionFactory sf = HibernateUtils.getSessionFactory();
            UserDAO userDAO = new UserDAOImpl(sf);
            userDAO.createUser(name, password, email, "visitor");
            msg = "Your account has been successfully created ! You can now upload your tracks !";
            return SUCCESS;
        } else {
            msg = "This email already exist";
            System.out.println("pageName :: " + pageName);
            if (pageName.equals("listen")) {
                return "inputListen";
            }else if (pageName.equals("index")) {
                return "inputIndex";
            }else if (pageName.equals("charts")) {
                return "inputCharts";
            }else{
                return NONE;
            }
        }
    }



    private boolean checkEmailDuplication(){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        UserDAO userDAO = new UserDAOImpl(sf);
        User SameEmailUser = userDAO.getUserByEmail(email);

        if (SameEmailUser == null){
            emailDuplicated = "notDuplicated";
            return true;
        }else{
            emailDuplicated = "duplicated";
            return false;
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String isEmailDuplicated() {
        return emailDuplicated;
    }

    public void setEmailDuplicated(String emailDuplicated) {
        this.emailDuplicated = emailDuplicated;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
}


