package com.msh.action;

import com.msh.dao.SoundDAO;
import com.msh.dao.SoundDAOImpl;
import com.msh.model.Sound;
import com.msh.model.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.json.annotations.JSON;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;

public class ListenAction extends ActionSupport implements SessionAware {

    private Map<String, Object> sessionMap;

    private String searchText;
    private String comment;
    private List<Sound> soundsByName;
    private List<Sound> soundsByUser;
    private File sound_file;
    private String msg;


    @Override
    public void setSession(Map<String, Object> map) {
        sessionMap = map;
    }

    public String execute() throws Exception {
        return SUCCESS;
    }


    public String retrieveSound() throws Exception{

        SoundDAO soundDAO = new SoundDAOImpl();
        soundsByUser = soundDAO.getSoundByUserName(searchText);

        soundsByName = soundDAO.getSoundBySoundName(searchText);

        /*for(int i = 0; i < sounds.size(); i++){
            FileOutputStream outputStream = new FileOutputStream("C:\\Users\\Cqssier\\Desktop\\" + sounds.get(i).getSound_name());
            byte[] byteArray = sounds.get(i).getSound();
            outputStream.write(byteArray);
        }*/

        if ( soundsByUser == null && soundsByName == null){
            msg = "no sound found";
            return INPUT;
        }else{
            /*msg = " results " + (soundsByUser.size() + soundsByName.size());*/
            msg = "notLogged";
            return SUCCESS;
        }
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public File getSound_file() {
        return sound_file;
    }

    public void setSound_file(File sound_file) {
        this.sound_file = sound_file;
    }

    public List<Sound> getSoundsByName() {
        return soundsByName;
    }

    public void setSoundsByName(List<Sound> soundsByName) {
        this.soundsByName = soundsByName;
    }

    public List<Sound> getSoundsByUser() {
        return soundsByUser;
    }

    public void setSoundsByUser(List<Sound> soundsByUser) {
        this.soundsByUser = soundsByUser;
    }

}

