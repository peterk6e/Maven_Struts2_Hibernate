package com.msh.action;

import com.google.gson.Gson;
import com.msh.dao.SoundDAO;
import com.msh.dao.SoundDAOImpl;
import com.msh.dao.UserDAO;
import com.msh.dao.UserDAOImpl;
import com.msh.listener.HibernateUtils;
import com.msh.model.Sound;
import com.msh.model.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.opensymphony.xwork2.Action.SUCCESS;

public class JsonAction extends ActionSupport {

    private Map<String, Object> sessionMap;
    private List<SoundJson> data;
    private String searchText;
    private String msg;
    private String jsonString;
    private List<String> AutocompleteNamesList;
    private  String AutocompJsonString;

    private List<SoundJson> SoundsByStyleList;
    private  String StyleJsonString;

    private String statusLoginJsonStr;


    public String execute() throws Exception {

        Gson gson = new Gson();
        data = getData();

                if (data == null) {
            msg = "no sound found";
        } else {
                    msg = " results : " + (data.size());
                   jsonString = gson.toJson(data);
        }
        return SUCCESS;
    }


    public List<SoundJson> getData() {

        List<Sound> soundList;
        SoundDAO soundDAO = new SoundDAOImpl();

        List<Sound> soundsByArtist = soundDAO.getSoundByArtistName(searchText);
        List<Sound> soundsByName = soundDAO.getSoundBySoundName(searchText);
        List<Sound> soundsByStyle = soundDAO.getSoundBySoundStyle(searchText);

        soundList = soundsByArtist;
        soundList.addAll(soundsByName);
        soundList.addAll(soundsByStyle);

        data = new ArrayList<SoundJson>();

        for (int i = 0; i < soundList.size(); i++) {

            SoundJson sj = new SoundJson(soundList.get(i).getSound_name(),
                    soundList.get(i).getArtist(),
                    soundList.get(i).getAlbum(),
                    soundList.get(i).getSound_location(),
                    soundList.get(i).getImage_location());

            data.add(sj);
        }
        return data;
    }

    public List<String> getAutocompleteNamesList() {
        SoundDAO soundDAO = new SoundDAOImpl();
        UserDAO userDAO = new UserDAOImpl(HibernateUtils.getSessionFactory());

        List<Sound> AllSoundsList = soundDAO.getAllSounds();
        List<User> AllUsersList = userDAO.getListOfUsers();

        List <String>  AutocompleteNamesList = new ArrayList<String>();
        String valueBefore = "";
        String currentValue ="";


        /*for(int i = 0; i < AllUsersList.size(); i++){

            currentValue = AllUsersList.get(i).getName();
            if(valueBefore.equals(currentValue)== false){ // test duplicate
                AutocompleteNamesList.add(currentValue);
            }
            valueBefore = currentValue;
        }*/

        for(int i = 0; i < AllSoundsList.size(); i++){
            AutocompleteNamesList.add(AllSoundsList.get(i).getSound_name());

            currentValue = AllSoundsList.get(i).getArtist();
            if(valueBefore.equals(currentValue)== false){ // test duplicate
                AutocompleteNamesList.add(currentValue);
            }
            valueBefore = currentValue;

        }
        return AutocompleteNamesList;
    }
    public void setAutocompleteNamesList(List <String> AutocompleteNamesList) {
        this.AutocompleteNamesList = AutocompleteNamesList;
    }

    public String getAutocompleteResult(){

        Gson gson = new Gson();
        AutocompleteNamesList = getAutocompleteNamesList();

        if (AutocompleteNamesList != null) {
            msg = " results : " + (AutocompleteNamesList.size());
            AutocompJsonString = gson.toJson(AutocompleteNamesList);
        }
        return SUCCESS;
    }


    public String getSoundsByStyleResult(){

        Gson gson = new Gson();
        SoundsByStyleList = getSoundsByStyleList();

        if (SoundsByStyleList != null) {
            msg = " results : " + (SoundsByStyleList.size());
            StyleJsonString = gson.toJson(SoundsByStyleList);
        }
        return SUCCESS;
    }
    public void setData(List<SoundJson> data) {
        this.data = data;
    }



    /*    public String getJson(){

     *//* Gson gson = new Gson();
        data = getData();

        System.out.println("getData :: " + data.toString()  );
        jsonString = gson.toJson(data);

        return SUCCESS;*//*
    }*/

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getAutocompJsonString() {
        return AutocompJsonString;
    }

    public void setAutocompJsonString(String autocompJsonString) {
        AutocompJsonString = autocompJsonString;
    }


    public List<SoundJson> getSoundsByStyleList() {

        List<Sound> soundList;
        SoundDAO soundDAO = new SoundDAOImpl();
        List<Sound> soundsByStyle = soundDAO.getSoundBySoundStyle(searchText);
        soundList = soundsByStyle;


        SoundsByStyleList = new ArrayList<SoundJson>();

        for (int i = 0; i < soundList.size(); i++) {

            SoundJson sj = new SoundJson(soundList.get(i).getSound_name(),
                    soundList.get(i).getArtist(),
                    soundList.get(i).getAlbum(),
                    soundList.get(i).getSound_location(),
                    soundList.get(i).getImage_location());

            SoundsByStyleList.add(sj);
        }
        return SoundsByStyleList;
    }

 /*   public String getStatus(){
        statusLoginJsonStr = getStatusLoginJsonStr();
        return SUCCESS;
    }

    public String getStatusLoginJsonStr() {
        Gson gson = new Gson();
        statusLoginJsonStr = gson.toJson("logged in");
        return statusLoginJsonStr;
    }*/





    public void setStatusLoginJsonStr(String statusLoginJsonStr) {
        this.statusLoginJsonStr = statusLoginJsonStr;
    }

    public void setSoundsByStyleList(List<SoundJson> soundsByStyleList) {
        SoundsByStyleList = soundsByStyleList;
    }

    public String getStyleJsonString() {
        return StyleJsonString;
    }

    public void setStyleJsonString(String styleJsonString) {
        StyleJsonString = styleJsonString;
    }



    /* class with parameters for the audio player Amplitudejs */
    class SoundJson {
        private String name;
        private String artist;
        private String album;
        private String url;
        private String cover_art_url;

        public SoundJson() {
        }

        public SoundJson(String name, String artist, String album, String url, String cover_art_url) {
            this.name = name;
            this.artist = artist;
            this.album = album;
            this.url = url;
            this.cover_art_url = cover_art_url;
        }
    }
}

