package com.msh.action;

import com.msh.dao.SoundDAO;
import com.msh.dao.SoundDAOImpl;
import com.msh.dao.UserDAO;
import com.msh.dao.UserDAOImpl;
import com.msh.listener.HibernateUtils;
import com.msh.model.Sound;
import com.msh.model.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.SessionFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;

public class UploadAction extends ActionSupport implements SessionAware {

    private Map<String, Object> sessionMap;

    private String name;
    private User user;

    private String sound_name;
    private String comment;
    private File sound_file;
    private String style;
    private String artist;
    private String album;
    private String msg;


    @Override
    public void setSession(Map<String, Object> map) {
        sessionMap = map;
    }

    public String execute() throws Exception {
        /*User user = (User)sessionMap.get("user");*/
        name = "USERNAME";
        return SUCCESS;
    }

    public String uploadSound() throws Exception {

        User user = (User)sessionMap.get("user");
        //String path = "C:/Users/Cqssier/Desktop/test/test.mp3";
        try {
            SoundDAOImpl soundDAO = new SoundDAOImpl();
            //soundDAO.saveSound(sound_name, path, user, comment);
            soundDAO.saveSound(sound_name, artist, album, sound_file, user, comment, style);

            msg = "sound uploaded !";

            return SUCCESS;
        } catch (Exception ex) {
            //throw ex;
            msg = "an error occurred, please try again.";
            return INPUT;

        }
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSound_name() {
        return sound_name;
    }

    public void setSound_name(String sound_name) {
        this.sound_name = sound_name;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public File getSound_file() {
        return sound_file;
    }

    public void setSound_file(File sound_file) {
        this.sound_file = sound_file;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}


