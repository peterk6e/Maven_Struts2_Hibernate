package com.msh.action;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.msh.dao.UserDAO;
import com.msh.dao.UserDAOImpl;
import com.msh.listener.HibernateUtils;
import com.msh.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class LoginAction extends ActionSupport implements SessionAware {

    private static final long serialVersionUID = -3434561352924343132L;
    private static final Logger LOGGER = LogManager.getLogger(LoginAction.class);
    private String name, password, msg;
    private SessionMap<String, Object> sessionMap;
    private String role;

    private InputStream inputStream;


    @Override
    public void setSession(Map<String, Object> map) {
        sessionMap = (SessionMap<String, Object>) map;
    }

    @Override
    public String execute() throws Exception {
        HttpSession session = ServletActionContext.getRequest().getSession(true);
        if (name != null) {
            SessionFactory sf = HibernateUtils.getSessionFactory();
            UserDAO userDAO = new UserDAOImpl(sf);
            User userDB = userDAO.getUserByCredentials(name, password);

            if (userDB == null) {
                msg = "Invalid Credentials";
                return INPUT;
            }
            else {
                role = userDB.getRole();
                sessionMap.put("user", userDB);

                return SUCCESS;
            }
        } else {
            User getSessionAttr = (User) session.getAttribute("user");
            if (getSessionAttr != null) {
                return SUCCESS;
            } else {
                return INPUT;
            }
        }
    }


    public String getStatus() {
        inputStream = new ByteArrayInputStream(
                "not logged".getBytes(StandardCharsets.UTF_8));
        return SUCCESS;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public SessionMap<String, Object> getSessionMap() {
        return sessionMap;
    }
    public void setSessionMap(SessionMap<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }


    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }
}