package com.msh.action;


import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class LogoutAction extends ActionSupport implements SessionAware {

    private static final Logger LOGGER = LogManager.getLogger(LoginAction.class);
    private SessionMap<String, Object> sessionMap;


    @Override
    public void setSession(Map<String, Object> map) {
        this.sessionMap = (SessionMap<String, Object>) map;
    }

    @Override
    public String execute() throws Exception {

        try { // test LOGGER
            sessionMap.remove("user");
            sessionMap.invalidate();
        }catch(Exception ex){
           /* LOGGER.warn(" WARN :: logout sessionMap ");*/
            throw ex;
        }
        return SUCCESS;
    }
}