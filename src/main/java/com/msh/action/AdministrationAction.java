package com.msh.action;

import com.msh.dao.SoundDAOImpl;
import com.msh.dao.UserDAO;
import com.msh.dao.UserDAOImpl;
import com.msh.listener.HibernateUtils;
import com.msh.model.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.SessionFactory;

import java.util.Map;

import static com.opensymphony.xwork2.Action.SUCCESS;

public class AdministrationAction extends ActionSupport implements SessionAware {

    private Map<String, Object> sessionMap;
    private String name;
    private String password;
    private String email;
    private String role;
    private String msg;


    @Override
    public void setSession(Map<String, Object> map) {
        sessionMap = map;
    }

    public String execute() throws Exception {
        return SUCCESS;
    }

    public String updateUser() throws Exception {

        SessionFactory sf = HibernateUtils.getSessionFactory();
        UserDAO userDAO = new UserDAOImpl(sf);
        userDAO.updateUser(name, password, email, role);
        msg = "user add / updated";
        return SUCCESS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}


