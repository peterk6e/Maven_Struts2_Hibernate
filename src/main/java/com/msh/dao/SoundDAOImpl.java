package com.msh.dao;

import com.msh.listener.HibernateUtils;
import com.msh.model.Sound;
import com.msh.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.query.Query;
import org.joda.time.LocalDate;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class SoundDAOImpl implements SoundDAO {

    @Override
    public void saveSound(String sound_name,String artist, String album, File sound_file, User user, String comment, String style)
            throws IOException, SQLException {

        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Sound sound = new Sound();

            // we save the file as byte array in the database
            FileInputStream inputStream = new FileInputStream(sound_file);
            byte[] byteArray = new byte[1024];
            inputStream.read(byteArray);
            sound.setSound(byteArray);

            //we also copy the file to a location and save the path.
            // => Only one of the 2 solutions is necessary.
            FileOutputStream outputStream = new FileOutputStream(
                    "C:/Users/Cqssier/intellij_workspace/Maven_Struts2_Hibernate/Web/mp3/" + sound_name);



            sound.setSound_name(sound_name);
            sound.setImport_date(LocalDate.now());
            sound.setComment(comment);
            sound.setUser(user);
            sound.setAlbum(album);
            sound.setArtist(artist);
            sound.setImage_location("../album-art/we-are-to-answer.jpg");
            sound.setSound_location("../../../../mp3/" + sound_name); //file.getPath()
            sound.setStyle(style);

            session.saveOrUpdate(sound);
            // session.save(sound);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public List<Sound> getSoundByUserName(String user_name) {

        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        User user = null;
        List<Sound> sounds = null;

        try {

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Sound> query = builder.createQuery(Sound.class);
            Root<User> userRoot = query.from(User.class);
            Root<Sound> soundRoot = query.from(Sound.class);
            query.select(soundRoot).distinct(true) ;

            query.where(builder.equal(userRoot.get("name"), user_name));

            Query<Sound> q = session.createQuery(query);
            sounds = q.getResultList();

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return sounds;
    }

    public List<Sound> getSoundBySoundName(String sound_name){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        List<Sound> sounds = null;

        try {

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Sound> query = builder.createQuery(Sound.class);
            Root<Sound> soundRoot = query.from(Sound.class);
            query.select(soundRoot).distinct(true) ;

            query.where(builder.equal(soundRoot.get("sound_name"), sound_name));

            Query<Sound> q = session.createQuery(query);
            sounds = q.getResultList();

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return sounds;
    }


    public List<Sound> getSoundBySoundStyle(String style){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        List<Sound> sounds = null;
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Sound> query = builder.createQuery(Sound.class);
            Root<Sound> soundRoot = query.from(Sound.class);
            query.select(soundRoot).distinct(true) ;

            query.where(builder.equal(soundRoot.get("style"), style));

            Query<Sound> q = session.createQuery(query);
            sounds = q.getResultList();

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return sounds;
    }

    public List<Sound>getAllSounds() {
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        List<Sound> sounds = null;
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Sound> query = builder.createQuery(Sound.class);
            Root<Sound> soundRoot = query.from(Sound.class);
            query.select(soundRoot).distinct(true);

            /*query.where(builder.equal(soundRoot.get("style"), style));*/

            Query<Sound> q = session.createQuery(query);
            sounds = q.getResultList();

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return sounds;
    }

    @Override
    public List<Sound> getSoundByArtistName(String artist) {

        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        List<Sound> sounds = null;
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Sound> query = builder.createQuery(Sound.class);
            Root<Sound> soundRoot = query.from(Sound.class);
            query.select(soundRoot).distinct(true);

            query.where(builder.equal(soundRoot.get("artist"), artist));

            Query<Sound> q = session.createQuery(query);
            sounds = q.getResultList();

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return sounds;
    }
}
