package com.msh.dao;

import com.msh.model.Sound;
import com.msh.model.User;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface SoundDAO {


    //void saveSound(String sound_name, String path, User user, String comment) throws IOException, SQLException;
    void saveSound(String sound_name, String artist, String album, File sound_file, User user, String comment, String style) throws IOException, SQLException;


    List<Sound> getSoundByUserName(String user_name);


     List<Sound> getSoundBySoundName(String sound_name);

    List<Sound> getSoundBySoundStyle(String sound_name);

    List<Sound>getAllSounds();

    List<Sound>getSoundByArtistName(String artist);

}
