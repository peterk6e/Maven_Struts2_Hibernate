package com.msh.dao;


import com.msh.model.User;


import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


import java.util.List;




public class UserDAOImpl implements UserDAO {
    private SessionFactory sf;

    public UserDAOImpl(SessionFactory sf) {
        //this.sf = HibernateUtils.getSessionFactory();
        this.sf = sf;
    }

    @Override
    public User getUserByCredentials(String name, String password) {
        Transaction transaction = null;
        User user = null;

      try {
          Session session = sf.openSession();
          transaction = session.beginTransaction();

          CriteriaBuilder builder = session.getCriteriaBuilder();
          CriteriaQuery<User> query = builder.createQuery(User.class);
          Root<User> root = query.from(User.class);

          query = query.select(root)
                  .where(builder.equal(root.get("name"), name),
                          builder.equal(root.get("password"), password));
                        //builder.equal(root.get("userId"), userId));

          Query<User> q = session.createQuery(query);
          user = q.getSingleResult();

            if (user != null) {
                System.out.println("User Retrieved from DB::" + user);
            }
      } catch (NoResultException e) {
          System.out.print("User:: no entity found");
          if (transaction != null) {
              transaction.rollback();
          }// transaction commit ??
      }
        return user;
    }



    @Override
    public List getListOfUsers() {
        List<User> users = null;
        Transaction transaction = null;
        try {
            Session session = sf.openSession();
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> query = builder.createQuery(User.class);
            Root<User> root = query.from(User.class);
            query.select(root);

            Query<User> q = session.createQuery(query);
            users = q.getResultList();

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return users;
    }

    @Override
    public void updateUser(String name, String password, String email, String role) {
        Transaction transaction = null;

        User user = new User(name, password, email, role);
        User searchedUser = getUserByCredentials(name, password);
        try {
            Session session = sf.openSession();
            transaction = session.beginTransaction();

            if (searchedUser != null) {
                //searchedUser.setName(user.getName());
                //searchedUser.setPassword(user.getName());
                searchedUser.setEmail(user.getEmail());
                searchedUser.setRole(user.getRole());
                //session.saveOrUpdate(user);
                session.update(searchedUser);
            } else {
                session.save(user);
            }
            //session.persist(user); // save in db and return void
            //session.save(user); //save in db and return a generated identifier
            //session.saveOrUpdate(user); // save or update
            //session.merge(user); // like saveOrUpdate but Object don't need to be attached to the session

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public void updateInfoUser(User currentUser, User updatedUser) {
        Transaction transaction = null;
        String currentName = currentUser.getName();
        String currentPassword = currentUser.getPassword();

        User searchedUser = getUserByCredentials(currentName, currentPassword);
        try {
            Session session = sf.openSession();
            transaction = session.beginTransaction();

                searchedUser.setName(updatedUser.getName());
                searchedUser.setPassword(updatedUser.getPassword());
                searchedUser.setEmail(updatedUser.getEmail());
                session.update(searchedUser);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public void createUser(String name, String password, String email, String role) {
            Transaction transaction = null;
            User newUser = new User(name, password, email, role);
            try {
                Session session = sf.openSession();
                transaction = session.beginTransaction();
                session.save(newUser);
                transaction.commit();
            } catch (Exception e) {
                e.printStackTrace();
                if (transaction != null) {
                    transaction.rollback();
                }
            }
    }



    @Override
    public User getUserByEmail(String email) {
        Transaction transaction = null;
        User user = null;

        try {
            Session session = sf.openSession();
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> query = builder.createQuery(User.class);
            Root<User> root = query.from(User.class);

            query = query.select(root)
                    .where(builder.equal(root.get("email"), email));
            Query<User> q = session.createQuery(query);
            user = q.getSingleResult();

            if (user != null) {
                System.out.println("this email already exists" + email);
            }
        } catch (NoResultException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return user;
    }
}






