package com.msh.dao;

import com.msh.model.User;

import java.util.List;

public interface UserDAO {

    User getUserByCredentials(String userId, String password);

    List getListOfUsers();

    void updateUser(String name, String password, String email, String role);

    void updateInfoUser(User currentUser, User updatedUser);

    void createUser(String name, String password, String email, String role);

    User getUserByEmail(String email);
}
