
$(document).ready(function () {

        checkValidEmail();
        checkRole();

         var searchText = sessionStorage.getItem('searchText');
    console.log("searchText :: " + searchText);

         if(searchText == null ){ //undefined
             playCurrentSong();
         }else{
             $("#searchText").val(searchText); /*set the value to the textField before searching*/
             searchData();
             sessionStorage.removeItem('searchText');
         }

    getAutoList(); /*get list of suggestion for input search*/
    });


/* when press "enter" */
$(function(){
    $('.input-group').keypress(function(e){
        if(e.which === 13) {
            searchData();
        }
    })
});

/*ajax call*/
function searchData() {
    var searchText = $("#searchText").val();
    $.ajax({
        type: "POST",
        url: "JsonAction.action",
        data : "searchText=" + searchText,
        dataType: "json"
        //success: function (jsonString) {
    }).done(function (jsonString) {

        emptyPlayer();

        setPlaylist(jsonString);

        console.log($('#div1'));
        checkMouse(); // see function.js (Amplitude)

        setSongs(JSON.parse(jsonString));

    }).fail(function() {
        alert("search failed");
        }
    );
}


function setSongs(param) {

        Amplitude.init({
            "bindings": {
                39: 'play_pause' /*spaceBar*/
            },
            "songs": param,
        });
}


function emptyPlayer(){

    /*to do again wrong solution*/
    /*init the player with empty song */
    Amplitude.init({
 "songs" :[{"name": "", "artist": "", "album": "", "url": "", "cover_art_url": ""}]
      /*  "songs" : [{}]*/
    });

    /*we empty the previous playlist*/
    $("#div1").empty();
}


/*function redirectionTest(){
    var searchText = $("#searchText").val();
    localStorage.myJson = searchText;
    window.location.href = 'http://localhost:8080/searchSound.action ';
}*/

function setPlaylist(param) {

    if (param.length === 2) {
        document.querySelector("#div1").innerHTML = "-- no sound found --";
    } else {

        /* for each song, we build a div tree with classes*/
        $.each(JSON.parse(param), function (i, item) {

            var div = document.createElement("div");
            div.setAttribute('class', 'song amplitude-song-container amplitude-play-pause');
            div.setAttribute('amplitude-song-index', i);
            //div.innerHTML = item.name;
            document.querySelector("#div1").appendChild(div);

            var innerDiv1 = document.createElement("div");
            innerDiv1.setAttribute('class', 'song-now-playing-icon-container');
            div.appendChild(innerDiv1);

            var innerDiv1_1 = document.createElement("div");
            innerDiv1_1.setAttribute('class', 'play-button-container');
            innerDiv1.appendChild(innerDiv1_1);

            var innerImg1_2 = document.createElement("img");
            innerImg1_2.setAttribute('class', 'now-playing');
            innerImg1_2.src = "../../js/amplitudejs-master/blue-playlist/img/now-playing.svg";
            innerDiv1.appendChild(innerImg1_2);

            var innerDiv2 = document.createElement("div");
            innerDiv2.setAttribute('class', 'song-meta-data');
            div.appendChild(innerDiv2);

            var innerDiv2_1 = document.createElement("span");
            innerDiv2_1.setAttribute('class', 'song-title');
            innerDiv2_1.innerHTML = item.name;
            innerDiv2.appendChild(innerDiv2_1);

            var innerDiv2_2 = document.createElement("span");
            innerDiv2_2.setAttribute('class', 'song-artist');
            innerDiv2_2.innerHTML = item.artist;
            innerDiv2.appendChild(innerDiv2_2);

            var innerDiv3 = document.createElement("span");
            innerDiv3.setAttribute('class', 'song-duration');
            getDuration(item.url).then(function(length) {
                innerDiv3.innerHTML = secondsToHms(length);
            });
            div.appendChild(innerDiv3);

            /*var innerDiv3_1 = document.createElement("span");
            innerDiv3_1.setAttribute('class', 'duration');
            innerDiv3.appendChild(innerDiv3_1);

            var innerDiv3_1_1 = document.createElement("span");
            innerDiv3_1_1.setAttribute('class', 'amplitude-duration-hours');
            innerDiv3_1_1.setAttribute('amplitude-main-duration-hours', 'true');
            innerDiv3_1.appendChild(innerDiv3_1_1);

            var innerDivColon = document.createElement("span");
            innerDivColon.innerHTML = ":";

            var innerDiv3_1_2 = document.createElement("span");
            innerDiv3_1_2.setAttribute('class', 'amplitude-duration-minutes');
            innerDiv3_1_2.setAttribute('amplitude-main-duration-minutes', 'true');
            innerDiv3_1.appendChild(innerDiv3_1_2);

            var innerDivColon = document.createElement("span");
            innerDivColon.innerHTML = ":";
            innerDiv3_1.appendChild(innerDivColon);

            var innerDiv3_1_3 = document.createElement("span");
            innerDiv3_1_3.setAttribute('class', 'amplitude-duration-seconds');
            innerDiv3_1_3.setAttribute('amplitude-main-duration-seconds', 'true');
            innerDiv3_1.appendChild(innerDiv3_1_3);*/

        });
    }
}

function storeSong() {

    var currentSong = Amplitude.getActiveSongMetadata();
    sessionStorage.setItem('currentSong', JSON.stringify(currentSong));

    var duration = +$(".amplitude-duration-seconds").html()
                   + $(".amplitude-duration-minutes").html() * 60
                   + $(".amplitude-duration-hours").html() * 3600;

    Amplitude.getSongPlayedPercentage();
    var seconds = duration * Amplitude.getSongPlayedPercentage() / 100;
    sessionStorage.seconds = seconds;

    var currentSongIndex = Amplitude.getActiveIndex();
    sessionStorage.currentSongIndex = currentSongIndex;

    var playlist = Amplitude.getActivePlaylist();
    sessionStorage.playlist = playlist;
    console.log("playlist :: " + playlist);
    console.log("stored");
}

window.onbeforeunload = function(){
    storeSong();
    /*return 'Are you sure you want to leave?';*/
};


function playCurrentSong() {

    console.log("playCurrentSong function");
console.log(" current :: " + localStorage.getItem('currentSong'));
        var currentSong = JSON.parse(sessionStorage.getItem('currentSong'));
        var seconds = sessionStorage.seconds;
        var index = sessionStorage.currentSongIndex;

        Amplitude.init({
           "songs": [currentSong],
        });
        Amplitude.setDebug( true );
        Amplitude.skipTo(seconds, index);

    /*localStorage.clear();
    console.log("local Storage cleared !")*/
}


/*get duration*/
function getDuration(src) {
    return new Promise(function(resolve) {
        var audio = new Audio();
        $(audio).on("loadedmetadata", function(){
            resolve(audio.duration);
        });
        audio.src = src;
    });
}
/*convert seconds to hhmmss*/
function secondsToHms(d) {
    d = Number(d);

    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    if (h === 0)
        return ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
    else
        return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}


/* autocomplete*/
var choices;
/*get list of suggestions*/
function getAutoList() {
    $.ajax({
        type: "GET",
        url: "AutocompleteAction.action",
        dataType: "json"
        //success: function (jsonString) {
    }).done(function (autocompJsonString) {
        choices = JSON.parse(autocompJsonString);
    })
}
/* suggest*/
$(function(){
    $('#searchText').autoComplete({
        minChars: 1,
        source: function(term, suggest){
            term = term.toLowerCase();
            var suggestions = [];
            for (i=0;i<choices.length;i++)
                if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
            suggest(suggestions);
        }
    });
});

/*get sound by style*/
function getSoundsByStyle(searchText) {
    console.log("searchText :: " + searchText);
    document.querySelector("#divChartsPlaylist").innerHTML =  searchText;

    $.ajax({
        type: "POST",
        url: "StyleAction.action",
        data : "searchText=" + searchText, // .toLowerCase()
        dataType: "json"
    }).done(function (StyleJsonString) {

        setStylePlaylist(StyleJsonString);

    }).fail(function() {
            alert("search failed");
        }
    );
}


function setStylePlaylist(param) {

    if (param.length === 2) {
        document.querySelector("#divChartsPlaylist").innerHTML = "-- no sound found --";
    } else {

        /* for each song, we build a div tree with classes*/
        $.each(JSON.parse(param), function (i, item) {

            var div = document.createElement("div");
            div.setAttribute('class', 'song amplitude-song-container amplitude-play-pause');
            div.setAttribute('amplitude-song-index', i);
            div.onclick = function() {
                redirect(item.name);
            };
            div.className = 'stylePlaylist';
            //div.innerHTML = item.name;
            document.querySelector("#divChartsPlaylist").appendChild(div);

            var innerDiv1_1 = document.createElement("div");
            innerDiv1_1.src = i + 1;
            div.appendChild(innerDiv1_1);

            var innerImg1_2 = document.createElement("img");
            innerImg1_2.setAttribute('amplitude-song-info', 'cover_art_url');
            innerImg1_2.setAttribute('amplitude-main-song-info', 'true');
            innerImg1_2.src = item.cover_art_url;
            innerImg1_2.className='imgStylePlaylist';
            div.appendChild(innerImg1_2);



            var innerDiv2_1 = document.createElement("span");
            innerDiv2_1.setAttribute('class', 'song-title');
            innerDiv2_1.innerHTML = item.name;
            innerDiv2_1.className = 'styleNamesPlaylist';
            div.appendChild(innerDiv2_1);


            var innerDiv2_2 = document.createElement("span");
            innerDiv2_2.setAttribute('class', 'song-artist');
            innerDiv2_2.innerHTML = item.artist;
            innerDiv2_2.className = 'styleNamesPlaylist';
            div.appendChild(innerDiv2_2);


            var innerDiv3 = document.createElement("span");
            innerDiv3.setAttribute('class', 'song-duration');
            innerDiv3.className = 'styleDurationPlaylist';
            getDuration(item.url).then(function(length) {
                innerDiv3.innerHTML = secondsToHms(length);
            });
            div.appendChild(innerDiv3);

        });
    }
}

/*authentication modal opened*/
function checkStatusLogin() {
    $.ajax({
        url: "getStatusAction.action",
        type: 'POST',
        dataType: 'text',
    }).done(function (data) {
        openSignMod();
        }).fail(function() {
            alert("get status failed");
        }
    );
}

/*modal open sign in / up*/
function openSignMod(elem){

        $(".menuSignTab").removeClass('active');
        $(".signTab").removeClass('active show');

    var id = $(elem).attr("id");

    if(id === "btn_signIn"){
        $('#signIn').addClass("active");
        $('#signInTab').addClass("active");
        $('#signInTab').addClass("show");
    }
    else{
        $('#signUp').addClass("active");
        $('#signUpTab').addClass("active");
        $('#signUpTab').addClass("show");
    }
    $('#sign_in_up_mod').modal('toggle');
    msg="";
}



function redirectToTab(elem) {
    /*window.location.href = 'userPage.action';*/

    var id = $(elem).attr("id");

    $(".menuTab").removeClass('active');
    $(".userTab").removeClass('active show');

    if(id === "collectionLink") {
        $('#menu1').addClass("active");
        $('#menu1').addClass("show");
        $('#collectionTab').addClass("active");
    }
    if(id === "yourTracksLink") {
        $('#menu2').addClass("active");
        $('#menu2').addClass("show");
        $('#yourTracksTab').addClass("active");
    }
    if(id === "profileLink") {
        $('#menu3').addClass("active");
        $('#menu3').addClass("show");
        $('#profileTab').addClass("active");
    }
}



/*show hide password in Profile user tab*/
$(function() {
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    })
});



function closeModal(){
    msg="";
    $('#sign_in_up_mod').modal('hide');
}

function checkValidEmail() {
    if(emailDuplicated == "duplicated"){
        openSignMod($('#btn_signUp'));
    }
    if (emailDuplicated == "notDuplicated"){
        alert(msg);
    }
    msg="";
}

function checkRole() {

    if(role != "admin"){
        $("#adminLinkDiv").hide();
    }
}