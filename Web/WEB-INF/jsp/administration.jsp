<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>



<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="./parts/meta.jsp" %>
<title>welcome</title>
<%@ include file="./parts/headerPlayer.jsp" %>
</head>
<body>
<%@ include file="./parts/userNavbar.jsp" %>

<div class="container" style="background: #c3e6cb; margin-top:30px">

    <div class="row">
        <div class="col-sm-4">
            -update users-
        </div>
        <div class="col-sm-8">
            <div>Add User:</div>
            <s:form action="updateUser" method="POST">
                <s:textfield name="name" label="User Name" required="true"  />
                <s:password name="password" label="Password" type="password" required="true"/>
                <s:textfield name="email" label="email" type ="email" required="true" />
                <s:select label="Select a role"
                          headerKey="admin" headerValue="admin"
                          list="#{'member':'member', 'visitor':'visitor'}"
                          name="role"
                          required="true"
                          value="visitor" />
                <s:submit value="submit"/>

                <s:property value="msg" />
            </s:form>
        </div>
    </div>
</div>


<%@ include file="./parts/footer.jsp" %>


</body>
</html>