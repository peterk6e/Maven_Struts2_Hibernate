<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>



<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="./parts/meta.jsp" %>
<title>user</title>
<%@ include file="./parts/headerPlayer.jsp" %>
</head>
<body>
<%@ include file="parts/userNavbar.jsp" %>




    <!-- Nav tabs -->
    <div class="container">
        <br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link menuTab active"  id="chartsTab" data-toggle="tab" href="#home"><h5>Charts</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link menuTab " id="collectionTab" data-toggle="tab" href="#menu1"><h5>Collection</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link menuTab" id="mySoundsTab" data-toggle="tab" href="#menu2"><h5>My Sounds</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link menuTab" id="profileTab" data-toggle="tab" href="#menu3"><h5>My Profile</h5></a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div id="home" class="container userTab tab-pane fade active show"><br>
                <%@ include file="./parts/charts.jsp" %>
            </div>
            <div id="menu1" class="container userTab tab-pane fade"><br>
                <%@ include file="./parts/collection.jsp" %>
            </div>
            <div id="menu2" class="container userTab tab-pane fade"><br>
                get my sound
            </div>
            <div id="menu3" class="container userTab tab-pane fade"><br>
                <%@ include file="./parts/profile.jsp" %>
            </div>

        </div>
    </div>





<%@ include file="./parts/footer.jsp" %>


</body>
</html>