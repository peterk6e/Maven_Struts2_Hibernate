<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>


<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="./parts/meta.jsp" %>
<title>upload</title>
<%@ include file="./parts/headerPlayer.jsp" %>
</head>
<body>
<%--<%@ include file="./parts/bigPic.jsp" %>--%>
<%@ include file="./parts/userNavbar.jsp" %>


<div class="container" style="margin-top:30px">
    <h3>Upload <s:property value="name"/></h3>

    <div class="row">
        <div class="col-sm-6">
            <img style="margin:auto" width = "80%" src="../../images/upload.jpg"/>
        </div>
        <div class="col-sm-6">
            <s:form action="uploadSound" method="POST" enctype="multipart/form-data" cssClass="strutsUploadForm">
                <s:textfield name="sound_name" placeholder="song's name" label="Sound Name" required="true"/>
                <s:textfield name="artist" placeholder="artist" label="Artist name" required="true"/>
                <s:textfield name="album" placeholder="album" label="album name" required="true"/>
                <s:textfield name="comment" placeholder="comment" label="comment"/>
                <s:select label="Select a style"
                headerKey="Alternative Rock" headerValue="Alternative Rock"
                list="#{'Ambient' : 'Ambient' ,
                'Classical' : 'Classical' ,
                'Country' : 'Country' ,
                'Dance & EDM' : 'Dance & EDM',
                'Dancehall' : 'Dancehall' ,
                'Deep House' : 'Deep House',
                'Disco' : 'Disco',
                'Drum & Bass' : 'Drum & Bass',
                'Dubstep' : 'Dubstep' ,
                'Electronic' : 'Electronic',
                'Folk & Singer-Songwriter' : 'Folk & Singer-Songwriter',
                'Hip-hop & Rap' : 'Hip-hop & Rap',
                'House' : 'House' ,
                'Indie' : 'Indie' ,
                'Jazz & Blue' : 'Jazz & Blue',
                'Latin' : 'Latin',
                'Metal' : 'Metal',
                'Piano' : 'Piano',
                'Pop' : 'Pop',
                'R&B & Soul' : 'R&B & Soul',
                'Reggae' : 'Reggae' ,
                'Reggaeton' : 'Reggaeton',
                'Rock' : 'Rock' ,
                'Soundtrack' : 'Soundtrack',
                'Techno' : 'Techno',
                'Trance' : 'Trance' ,
                'Trap' : 'Trap',
                'Triphop' : 'Triphop',
                'World' : 'World' ,
                'Audiobooks' : 'Audiobooks',
                'Business' : 'Business',
                'Comedy' : 'Comedy',
                'Entertainment' : 'Entertainment',
                'Learning' : 'Learning',
                'News & Politics' : 'News & Politics',
                'Religion & Spirituality' : 'Religion & Spirituality',
                'Science' : 'Science',
                'Sports' : 'Sports',
                'Storytelling' : 'Storytelling',
                'Technology' : 'Technology' }"
                name="style"
                required="true"
                value="Alternative Rock"/>

                <s:file label="File" name="sound_file" required="true" cssClass="strutsUploadFormMargin"/>

                <s:submit value="Upload"></s:submit>
                <s:property value="msg"/>
            </s:form>
        </div>
    </div>
</div>


<%@ include file="./parts/footer.jsp" %>


</body>
</html>