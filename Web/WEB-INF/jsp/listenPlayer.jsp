<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="grid-x" id=""><%--blue-playlist-container--%>
    <div class="large-10 medium-12 small-11 large-centered medium-centered small-centered cell" id="amplitude-player">
        <div class="grid-x">
            <div class="large-5 medium-5 small-12 cell" id="amplitude-left">
                <img amplitude-song-info="cover_art_url" amplitude-main-song-info="true"/>
                <div id="player-left-bottom">
                    <div id="time-container">
				        <span class="current-time">
                            <span class="amplitude-current-hours" amplitude-main-current-hours="true"></span>:<span
                                  class="amplitude-current-minutes" amplitude-main-current-minutes="true"></span>:<span
                                  class="amplitude-current-seconds" amplitude-main-current-seconds="true"></span>
				        </span>
                        <div id="progress-container">
                            <input type="range" class="amplitude-song-slider" amplitude-main-song-slider="true"/>
                            <progress id="song-played-progress" class="amplitude-song-played-progress"
                                      amplitude-main-song-played-progress="true"></progress>
                            <progress id="song-buffered-progress" class="amplitude-buffered-progress"
                                      value="0"></progress>
                        </div>
                        <span class="duration">
                            <span class="amplitude-duration-hours" amplitude-main-duration-hours="true"></span>:<span
                                class="amplitude-duration-minutes" amplitude-main-duration-minutes="true"></span>:<span
                                class="amplitude-duration-seconds" amplitude-main-duration-seconds="true"></span>
				        </span>
                    </div>

                    <div id="control-container">
                        <div id="repeat-container">
                            <div class="amplitude-repeat" id="repeat"></div>
                            <div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle"></div>
                        </div>

                        <div id="central-control-container">
                            <div id="central-controls">
                                <div class="amplitude-prev" id="previous"></div>
                                <div class="amplitude-play-pause" amplitude-main-play-pause="true"
                                     id="play-pause"></div>
                                <div class="amplitude-next" id="next"></div>
                            </div>
                        </div>

                        <div id="volume-container">
                            <div class="volume-controls">
                                <div class="amplitude-mute amplitude-not-muted"></div>
                                <input type="range" class="amplitude-volume-slider"/>
                                <div class="ms-range-fix"></div>
                            </div>
                            <div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle-right"></div>
                        </div>
                    </div>


                    <div id="meta-container">
                        <span amplitude-song-info="name" amplitude-main-song-info="true" class="song-name"></span>

                        <div class="song-artist-album">
                            <span amplitude-song-info="artist" amplitude-main-song-info="true"></span>
                            <span amplitude-song-info="album" amplitude-main-song-info="true"></span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- playlist -->
            <%@ include file="parts/playlist.jsp" %>

        </div>
    </div>
</div>

