<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>



<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="./parts/meta.jsp" %>
<title>charts</title>
<%@ include file="./parts/headerPlayer.jsp" %>
</head>
<body>



<%@ include file="./parts/navbar.jsp" %>

<%@ include file="./parts/charts.jsp" %>


<%@ include file="./parts/footer.jsp" %>


<script>
    $('#pageName').val("charts"); /*set the pageName*/
</script>
</body>
</html>