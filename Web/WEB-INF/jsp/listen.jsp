<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html lang="en">
<head>

<%@ include file="./parts/meta.jsp" %>
<title>listen</title>
<%@ include file="./parts/headerPlayer.jsp" %>

</head>
<body>
<%--<%@ include file="./parts/bigPic.jsp" %>--%>
<%@ include file="./parts/navbar.jsp" %>


<div class="container" style="margin-top:30px">

    <%--<%@ include file="./parts/searchListen.jsp" %>--%>
    <%@ include file="listenPlayer.jsp" %>


</div>


<%--<%@ include file="./parts/footer.jsp" %>--%>

<script>
    $('#pageName').val("listen"); /*set the pageName*/
</script>
</body>
</html>
