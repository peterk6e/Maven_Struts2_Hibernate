<div class="container" style="margin-top:15px; padding:15px; background: #f2f2f2">
    <p> Playlist :</p>
    <div class="row" >
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px" src="https://i1.sndcdn.com/artworks-000196743536-97ge46-t500x500.jpg">
            <a class="nav-link playlist_collection" id="chill" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Chill</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://contentpl-a.akamaihd.net/images/playlists/image/medium/fe163d73f1351ec34fa97854ae4e5bef.jpg">
            <a class="nav-link playlist_collection" id="hip-hop" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Hip-hop</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQI96o3-7AlWThgoQ1YmVjO7MfhwFLr_veH9kanruNJjP6HwvA5Zg">
            <a class="nav-link playlist_collection" id="techno" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Techno</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKmxvhuQg2vtpPaVP7AMl21xJPPQLsyMiDQdEmrwnCd6FfSz78">
            <a class="nav-link playlist_collection" id="jazz and blues" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Jazz & Blues</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQy0QWjwKrxPCbhfXgeF_oDcCwsKc9zbU35p2bYKk8y_PMTshs7WA">
            <a class="nav-link playlist_collection" id="electronic" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Electronic</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2Fw4q2m1-FpRoZ0cb4rzuPmbOEz9wowt6I5cSTVjMvwastqJlXA">
            <a class="nav-link playlist_collection" id="classical" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Classical</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://whythebeatlesarestillrelevant.files.wordpress.com/2018/07/guitar-intro.jpg?w=500">
            <a class="nav-link playlist_collection" id="rock" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Rock</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRt00Sf1nBIfjhnOuCxOL9UhkCKKlZHtobnCjGecvVuoiHqOmSd">
            <a class="nav-link playlist_collection" id="r&b & soul" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;R&B & Soul</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQE340C7WTY85J3ZXmho5EfHTnY0vMuk2dcfTsFgUqUSXr07r4O">
            <a class="nav-link playlist_collection" id="pop" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Pop</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px" src="https://i1.sndcdn.com/artworks-000199553919-a3hjn1-t500x500.jpg">
            <a class="nav-link playlist_collection" id="drum and bass" href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Drum and Bass</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSE0_YNA6PTwQYE5vu6-9YR8QRrrcqKzglVMwGOxq3Grx67U2pNTQ">
            <a class="nav-link playlist_collection" id="folk"   href="searchSound.action"><i class="fas fa-headphones-alt"></i>&nbsp;Folk</i>
            </a>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6">
            <img width="150px"
                 src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRABXT_UTCaktJzm82H3PjBjnnTQPCdfZnJj3VF08gTkA6DFg3">
            <a class="nav-link playlist_collection"   id="reggae"  href="searchSound.action" ><i class="fas fa-headphones-alt"></i>&nbsp;Reggae</i>
            </a>
        </div>
    </div>
</div>
<script>


    $(".playlist_collection").click(function(){
        let searchText;

        searchText = $(this).attr("id");

        localStorage.myJson = searchText;
        /*window.location.href = 'searchSound.action ';*/
    });


</script>