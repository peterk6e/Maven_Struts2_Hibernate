<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<div class="row" style=" margin-top: 25px">
    <div class="col-8">
        <%--<s:form action="searchSound" method="POST" > &lt;%&ndash; searchSound&ndash;%&gt;
        <div class="input-group mb-3" style="background: #ddd; margin-top : 25px">
            <s:textfield class="form-control" name="searchText" aria-describedby="basic-addon2" placeholder="search for artists, bands, tracks"/>
            <s:submit  type="button" cssClass="btn btn-outline-secondary">
                <i class="fas fa-search"></i></s:submit>
        </div>
        </s:form>--%>

        <%--Ajax method--%>
        <div class="input-group ">
            <input id ="searchText" type="text" class="form-control" placeholder="search for artists, bands, tracks" aria-describedby="basic-addon3">
            <div class="input-group-append">
                <button onclick="redirect()"  style="height: 38px" class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
            </div>
        </div>

    </div>

        <div class="col-1" style="text-align: center; margin: auto">
            or
        </div>


        <div class="col-3" style="margin:auto">
            <button onclick="checkStatusLogin()" class="update_own">Upload your own</button>
        </div>
    </div>



    <%--<div class="col-sm-3" style="border-left: 1px solid black">
        <b> Sounds by name:</b>
        <s:iterator var="i" step="1" value="soundsByName">
            <br>
            <s:property value="sound_name"/>
        </s:iterator>
    </div>

    <div class="col-sm-3" style="border-left: 1px solid black">
        <b> Sounds by user:</b>
        <s:iterator var="i" step="1" value="soundsByUser">
            <br>
            <s:property value="sound_name" />
        </s:iterator>
    </div>
--%>



</div>
<script>
    document.getElementById("searchText").focus();


    $(function(){
        $('.btn .btn-outline-secondary').keypress(function(e){
            if(e.which === 13) {
                searchData();
            }
        })
    });
</script>
