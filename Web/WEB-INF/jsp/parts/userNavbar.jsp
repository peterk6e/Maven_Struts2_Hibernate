<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
    <a class="navbar-brand" href="#">SoundLib'</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <%--<li class="nav-item">
                <a class="nav-link" href="listen">listen</a>
            </li>--%>
                <li class="nav-item">
                    <a class="nav-link" href="upload.action"><i class="fas fa-upload">&nbsp;Upload</i></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" onclick="redirectToTab(this)"><i class="far fa-star">&nbsp;Charts</i></a>
                </li>


            <%--struts form--%>
            <%-- <li class="col-12 ">
                   <s:form action="JsonAction" method="POST" >
                       <div class="input-group " > <!--style.css margin-nottom div.group-input: bootstrap overridden&ndash-->
                           <s:textfield class="form-control" name="searchText" aria-describedby="basic-addon2"
                                        placeholder="search for artists, bands, tracks"/>
                           <s:submit type="button" cssClass="btn btn-outline-secondary">
                               <i class="fas fa-search"></i></s:submit>
                   </s:form>
                   </li>--%>

            <%--Ajax method--%>
            <div class="input-group col-12">
                <input id ="searchText" type="text" class="form-control" placeholder="search for artists, bands, tracks" aria-describedby="basic-addon3">
                <div class="input-group-append">
                    <button onclick="searchData()" style="height: 38px" class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
                </div>
            </div>

        </ul>

        <ul class=" navbar-nav ml-auto ">

            <li id="adminLinkDiv" class="nav-item">
                <a class="nav-link" href="administration.action"><i style="color: #71dd8a" class="fas fa-unlock-alt">&nbsp;administration</i></a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link" href="#"  role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-envelope"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">no messages</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link" href="#"  role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bell"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">no activities</a>
                </div>
            </li>



            <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user-tie">&nbsp;</i><s:property value="name"/>&nbsp;<i class="fas fa-sort-down"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" id="collectionLink" onclick="redirectToTab(this)">Collection</a>
                    <a class="dropdown-item" id="profileLink" onclick="redirectToTab(this)">Profile</a>
                    <a class="dropdown-item" id="yourTracksLink" onclick="redirectToTab(this)">Your tracks</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" id="contactLinks" onclick="redirectToTab(this)">contact us</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="color: #cca3c8" href="logout">logout</a>
                </div>
            </li>
            <%--<li class=" nav-item">
                <a class="nav-link" href="logout">logout</a>
            </li>--%>
        </ul>
    </div>
</nav>


<%@ include file="/WEB-INF/jsp/parts/modals.jsp" %>

<script >
    var role='<s:property value="role" />';
</script>


