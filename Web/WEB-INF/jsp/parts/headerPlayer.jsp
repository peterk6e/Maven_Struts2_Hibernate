<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">




<script src="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700" type="text/css"></script>
<%--
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
      integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
--%>

<link type="text/css" href="../../fonts/all.css" rel="stylesheet"> <!--Awesome-font load all styles -->


<!-- ../ -->
<link rel="stylesheet" type="text/css" href="../../js/amplitudejs-master/blue-playlist/css/app.css"/>
<link rel="stylesheet" type="text/css" href="../../js/amplitudejs-master/blue-playlist/css/foundation.min.css"/>


<link rel="stylesheet" href="../../js/bootstrap/bootstrap.min.css"/>
<link rel="stylesheet" href="../../css/jquery.auto-complete.css"/>
<link rel="stylesheet" href="../../css/style.css"/>



<script type="text/javascript" src="../../js/bootstrap/jquery-3.3.1.min.js"></script>
<!-- ../ -->
<script type="text/javascript" src="../../js/amplitudejs-master/blue-playlist/js/jquery.js"></script>
<script  type="text/javascript" src="../../js/bootstrap/popper.min.js"></script>
<script  type="text/javascript" src="../../js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/amplitudejs-master/blue-playlist/js/foundation.min.js"></script>
<script type="text/javascript" src="../../js/amplitudejs-master/blue-playlist/js/readJson.js"></script>

<script type="text/javascript" src="../../js/amplitudejs-master/blue-playlist/js/functions.js"></script>

<script type="text/javascript" src="../../js/amplitudejs-master/dist/amplitude.js"></script>

<script type="text/javascript" src="../../js/autocomplete/jquery.auto-complete.min.js"></script>

<!-- ../ -->



