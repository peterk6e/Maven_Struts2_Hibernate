<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%-- modals--%>
<div id="sign_in_up_mod" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">

                <div class="container">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link menuSignTab active" id="signIn" data-toggle="tab" href="#signInTab">
                                <h6>Sign in</h6></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menuSignTab" id="signUp" data-toggle="tab" href="#signUpTab">
                                <h6>Sign up</h6></a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="signInTab" class="container signTab tab-pane fade active show"><br>
                            <%@ include file="/WEB-INF/jsp/parts/login.jsp" %>

                        </div>
                        <div id="signUpTab" class="container signTab tab-pane fade "><br>
                            <%@ include file="/WEB-INF/jsp/parts/signUp.jsp" %>
                        </div>

                        <div class="modal-footer">
                            <div style="color:#0056b3; text-align: left;" id="msg"><s:property value="msg"/></div>
                            <button type="button" class="btn btn-default" onclick="closeModal()">Close</button>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script >
  var emailDuplicated='<s:property value="emailDuplicated" />';
  var msg='<s:property value="msg" />';
</script>
<%--<!-- Modal sign in -->
<div id="sign_in_mod" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <%@ include file="/WEB-INF/jsp/parts/login.jsp" %>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>--%>

<!-- Modal sign create account -->
<%--<div id="sign_up_mod" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <%@ include file="/WEB-INF/jsp/parts/signUp.jsp" %>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>--%>