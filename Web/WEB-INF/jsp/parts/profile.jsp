<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<div class="container" style="margin-top:30px ">
    <div class="row">
        <div class="col-sm-4">
            <div><h4>Your profile info :</h4></div>
            <br>
            <s:form action="updateInfoUser" method="POST">
                <s:label >Name :</s:label>
                <s:textfield  class="form-control" name="name" label="User Name" required="true"/>
                <s:label >Password :</s:label>
                <s:textfield id="password-field" class="form-control" name="password" label="Password" type="password" required="true"/>
                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                <s:label >Email :</s:label>
                <s:textfield  class="form-control" name="email" label="email" type="email" required="true"/>
                <s:submit value="Update my info"/>

            </s:form>
            <s:property value="msg"/>
            <%--
            <s:property value="name"/>
            <s:property value="password"/>
            <s:property value="email"/>--%>
        </div>
    </div>
</div>
