<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>
  <s:form action="createUser" method="POST">
    <s:textfield name="name" placeholder="User Name" required="true"/>
    <s:password name="password" placeholder="Password" type="password" required="true"/>
    <s:textfield name="email" placeholder="email" type="email" required="true"/>

    <s:hidden id="pageName" name="pageName" value="index"/>  <%--we pass this parameter to know what is the current jsp,
to redirect to the same jsp.p--%>

    <s:submit value="Create account"/>


  </s:form>

</div>