<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="footer sticky-bottom"><%--1--%>
    <%--<div class="grid-x" id="blue-playlist-container">--%><%--2--%>
        <%--<div class="large-12 medium-12 small-11 large-centered medium-centered small-centered cell">--%> <%--3--%>
            <div id="amplitude-player" class="mini"><%--4--%>
                <div class="grid-x"><%--5--%>

                    <div class="large-3 medium-3 small-3 cell " id="amplitude-left"> <%--left part--%>
                        <div id="control-container-mini">
                            <div id="repeat-container" class="inline-mini">
                                <div class="amplitude-repeat amplitude-repeat-off inline-mini " id="repeat-mini"></div>
                                <div class="amplitude-shuffle amplitude-shuffle-off inline-mini" id="shuffle-mini"></div>
                            </div>

                            <div id="central-control-container" class="inline-mini">
                                <div id="central-controls">
                                    <div class="amplitude-prev inline-mini" id="previous-mini"></div>
                                    <div class="amplitude-play-pause inline-mini" amplitude-main-play-pause="true"
                                         id="play-pause-mini"></div>
                                    <div class="amplitude-next inline-mini" id="next-mini"></div>
                                </div>
                            </div>
                        </div>

                    </div>







                    <div class="large-6 medium-6 small-6 cell" id="amplitude-right">
                        <div id="time-container" class="time-mini">
                            <div> <%----%>
                                    <span class="current-time">
                                        <span class="amplitude-current-hours"
                                              amplitude-main-current-hours="true"></span>:
                                  <span class="amplitude-current-minutes"
                                        amplitude-main-current-minutes="true"></span>:<span
                                            class="amplitude-current-seconds" amplitude-main-current-seconds="true"></span>
                                </span>
                                <div id="progress-container">
                                    <input type="range" class="amplitude-song-slider"
                                           amplitude-main-song-slider="true"/>
                                    <progress id="song-played-progress" class="amplitude-song-played-progress"
                                              amplitude-main-song-played-progress="true"></progress>
                                    <progress id="song-buffered-progress" class="amplitude-buffered-progress"
                                              value="0"></progress>
                                </div>
                                <span class="duration">
                                    <span class="amplitude-duration-hours"
                                          amplitude-main-duration-hours="true"></span>:<span
                                        class="amplitude-duration-minutes" amplitude-main-duration-minutes="true"></span>:<span
                                        class="amplitude-duration-seconds"
                                        amplitude-main-duration-seconds="true"></span>
                                        </span>
                            </div>
                        </div>
                    </div> <%--Right--%>

                    <div class="large-2 medium-2 small-2 cell meta-container-mini" id="amplitude-right"><%--song info--%>
                        <%--<div id="meta-container" >--%>
                            <span amplitude-song-info="name" amplitude-main-song-info="true"
                                  class="song-name"></span>

                            <div class="song-artist-album " id="song-artist-album-mini">
                                <span amplitude-song-info="artist" id="artist-mini" amplitude-main-song-info="true"></span>
                                <span amplitude-song-info="album" id="album-mini" amplitude-main-song-info="true"></span>
                            </div>
                        <%--</div>--%>
                    </div><%--end song info--%>


                    <div class="large-1 medium-1 small-1 cell" id="amplitude-right">
                    <div id="volume-container-mini"><%--volume container--%>
                            <div class="volume-controls " >
                                <div  id="sound-mini" class="amplitude-mute amplitude-not-muted "></div>
                                <input type="range" class="amplitude-volume-slider" id="sound-slider-mini"/>
                                <div class="ms-range-fix"></div>
                            </div>
                            <div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle-right"></div>
                        </div><%--end volume container--%>
                    </div>

                 </div> <%-- 5 grid-x--%>
             </div><%--4 amplitude player--%>
        <%--</div>--%> <%--3--%>
    <%--</div>--%><%--2--%>
</div><%--1--%>


        <script type="text/javascript">
        </script>

