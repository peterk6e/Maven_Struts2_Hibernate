<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<div class="container" style="margin-top:30px ">

    <div class="row">
        <div class="col-6">
            <div class="btn-group" >
                <button class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink1"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Top 50
                    <span class="caret"></span>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1">
                    <a class="dropdown-item" href="#">Top 50</a>
                    <a class="dropdown-item" href="#">News & hot</a>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="btn-group"  >
                <button class="btn btn-secondary dropdown-toggle" href="#"  data-placement="bottom" role="button" id="dropdownMenuLink2"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    All music genres
                    <span class="caret"></span>
                </button>
                <div id ="dropdownStyle" class="dropdown-menu w-100" aria-labelledby="dropdownMenuLink2">
                    <a class="dropdown-item" href="#">All music genres</a>
                    <a class="dropdown-item" href="#">All audio genres</a>
                    <div class="dropdown-divider"></div>
                    <h6 class="dropdown-header">MUSIC</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Alternative Rock</a>
                    <a class="dropdown-item" href="#">Ambient</a>
                    <a class="dropdown-item" href="#">Classical</a>
                    <a class="dropdown-item" href="#">Country</a>
                    <a class="dropdown-item" href="#">Dance & EDM</a>
                    <a class="dropdown-item" href="#">Dancehall</a>
                    <a class="dropdown-item" href="#">Deep House</a>
                    <a class="dropdown-item" href="#">Disco</a>
                    <a class="dropdown-item" href="#">Drum & Bass</a>
                    <a class="dropdown-item" href="#">Dubstep</a>
                    <a class="dropdown-item" href="#">Electronic</a>
                    <a class="dropdown-item" href="#">Folk & Singer-Songwriter</a>
                    <a class="dropdown-item" href="#">Hip-hop & Rap</a>
                    <a class="dropdown-item" href="#">House</a>
                    <a class="dropdown-item" href="#">Indie</a>
                    <a class="dropdown-item" href="#">Jazz & Blue</a>
                    <a class="dropdown-item" href="#">Latin</a>
                    <a class="dropdown-item" href="#">Metal</a>
                    <a class="dropdown-item" href="#">Piano</a>
                    <a class="dropdown-item" href="#">Pop</a>
                    <a class="dropdown-item" href="#">R&B & Soul</a>
                    <a class="dropdown-item" href="#">Reggae</a>
                    <a class="dropdown-item" href="#">Reggaeton</a>
                    <a class="dropdown-item" href="#">Rock</a>
                    <a class="dropdown-item" href="#">Soundtrack</a>
                    <a class="dropdown-item" href="#">Techno</a>
                    <a class="dropdown-item" href="#">Trance</a>
                    <a class="dropdown-item" href="#">Trap</a>
                    <a class="dropdown-item" href="#">Triphop</a>
                    <a class="dropdown-item" href="#">World</a>
                    <div class="dropdown-divider"></div>
                    <h6 class="dropdown-header">AUDIO</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Audiobooks</a>
                    <a class="dropdown-item" href="#">Business</a>
                    <a class="dropdown-item" href="#">Comedy</a>
                    <a class="dropdown-item" href="#">Entertainment</a>
                    <a class="dropdown-item" href="#">Learning</a>
                    <a class="dropdown-item" href="#">News & Politics</a>
                    <a class="dropdown-item" href="#">Religion & Spirituality</a>
                    <a class="dropdown-item" href="#">Science</a>
                    <a class="dropdown-item" href="#">Sports</a>
                    <a class="dropdown-item" href="#">Storytelling</a>
                    <a class="dropdown-item" href="#">Technology</a>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6">

            <div id="divChartsPlaylist">-Audio style-</div>
        </div>
    </div>
</div>


<script>

    $(".dropdown-menu a").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');

        getSoundsByStyle(selText);
    });


    function searchData(){
        var searchText = $("#searchText").val();
        //localStorage.myJson = searchText;
        sessionStorage.setItem('searchText', searchText);
        window.location.href = 'searchSound.action';
    }
    function redirect(param){
        var searchText = param;
        //localStorage.myJson = searchText;
        sessionStorage.setItem('searchText', searchText);
        window.location.href = 'searchSound.action';
    }
</script>