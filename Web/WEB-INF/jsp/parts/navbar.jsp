<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
    <a class="navbar-brand" href="home.action">SoundLib'</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <%--<li class="nav-item">
                <a class="nav-link" href="listen">listen</a>
            </li>--%>
                <li class="nav-item">
                    <a class="nav-link" onclick="checkStatusLogin()"><i class="fas fa-upload">&nbsp;Upload</i></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="charts.action"><i class="far fa-star">&nbsp;Charts</i></a>
                </li>


            <%--struts form--%>
            <%-- <li class="col-12 ">
                   <s:form action="JsonAction" method="POST" >
                       <div class="input-group " > <!--style.css margin-nottom div.group-input: bootstrap overridden&ndash-->
                           <s:textfield class="form-control" name="searchText" aria-describedby="basic-addon2"
                                        placeholder="search for artists, bands, tracks"/>
                           <s:submit type="button" cssClass="btn btn-outline-secondary">
                               <i class="fas fa-search"></i></s:submit>
                   </s:form>
                   </li>--%>

            <%--Ajax method--%>
            <div class="input-group col-12">
                <input id ="searchText" type="text" class="form-control" placeholder="search for artists, bands, tracks" aria-describedby="basic-addon3">
                <div class="input-group-append">
                    <button onclick="searchData()" style="height: 38px" class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
                </div>
            </div>

        </ul>

        <ul class=" navbar-nav ml-auto ">

            <li class=" nav-item"> <a class="nav-link" href="logout"></a></li>

            <%--<form class="form-inline">--%>
                <button class="btn sign_in_butt sign_nav_btn" data-toggle="modal" id="btn_signIn" onclick="openSignMod(this)">
                    Sign in
                </button>

                <button class=" btn create_account_butt sign_nav_btn" data-toggle="modal" id="btn_signUp" onclick="openSignMod(this)">
                    Create account
                </button>
            <%--</form>--%>

        </ul>
    </div>
</nav>

<%@ include file="/WEB-INF/jsp/parts/modals.jsp" %>





