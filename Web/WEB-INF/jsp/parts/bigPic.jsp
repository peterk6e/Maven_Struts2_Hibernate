<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<div class="containerTop text-center ">
    <div class="elementsTop">

        <button class="create_account_butt margin_btn_bp" data-toggle="modal" id="btn_signUp" onclick="openSignMod(this)" >Create account</button>
        <button class="sign_in_butt margin_btn_bp"  data-toggle="modal" id="btn_signIn" onclick="openSignMod(this)" >Sign in</button>

        <div class="titleTop">
            <h1> Connect on SoundLib'</h1>
            <p>Discover, stream, and share a constantly expanding mix of music </p>
            <p> from emerging and major artists around the world.</p>
            <button class="sign_in_for_free_butt"  data-toggle="modal" onclick="openSignMod(this)">Sign up for free</button>
        </div>
    </div>

    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="../../images/background.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="../../images/bg2.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="../../images/bg3.jpg" alt="Third slide">
            </div>
        </div>
    </div>
    <%--<img src="../../images/background.jpg" alt="top" style="width:100%">--%>
</div>


<%@ include file="/WEB-INF/jsp/parts/modals.jsp" %>

