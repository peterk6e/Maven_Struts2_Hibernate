<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="WEB-INF/jsp/parts/meta.jsp" %>
    <title>Home Page</title>
    <%@ include file="WEB-INF/jsp/parts/headerPlayer.jsp" %>
    <%--<%@ include file="WEB-INF/jsp/parts/header.jsp" %>--%>



</head>
<body>
    <div class="container">
        <%@ include file="WEB-INF/jsp/parts/bigPic.jsp" %>
        <%@ include file="WEB-INF/jsp/parts/searchListen.jsp" %>

        <%--<%@ include file="/WEB-INF/jsp/listenPlayer.jsp" %>--%>

    </div>




<%@ include file="WEB-INF/jsp/parts/footer.jsp" %>
<%--
    <s:param name="pageName" value="index" /> &lt;%&ndash; we pass this parameter to know what is the current jsp,
    to redirect to the same jsp. Same parameter is added on index.jsp&ndash;%&gt;--%>

    <script>
        $('#pageName').val("index"); /*set the pageName*/

        function redirect() {
            var searchText = $("#searchText").val();
            //localStorage.myJson = searchText;
            sessionStorage.setItem('searchText', searchText);
            window.location.href = 'searchSound.action';
        }
    </script>
</body>
</html>